import math
import numpy
import matplotlib.pyplot as plt
from scipy.interpolate import Rbf

data = [0.1000, 0.1455, 0.1909, 0.2364, 0.2818, 0.3273, 0.3727, 0.4182, 0.4636, 0.5091, 0.5545, 0.6000, 0.6455, 0.6909,
        0.7364, 0.7818, 0.8273, 0.8727, 0.9182, 0.9636]

c1 = 0.9
r1 = 0.4
c2 = 0.55
r2 = 0.1


def get_desired_result(x):
    return (1 + 0.6 * math.sin(2 * math.pi * x / 0.7) + 0.3 * math.sin(2 * math.pi * x)) / 2


def get_desired_results_in_array(x_array):
    result = []
    for x in x_array:
        result.append(get_desired_result(x))

    return result


def get_gaus_output(x, c, r):
    # return activate(Rbf(x, c, r, function='gaussian')
    # exp(-(x - c) ^ 2 / (2 * r ^ 2)))
    return activate(math.exp((math.pow((x-c), 2))/math.pow(2*r, 2)))
    # return math.exp((math.pow(-(x - c), 2)) / math.pow(2 * r, 2))


def get_random_float():
    rnd = numpy.random.randn(1)[0]
    return round(rnd, 2)


def get_data_for_validating_results():
    return numpy.arange(0, 1, 0.05)


def activate(x):
    return 1/(1+math.exp(-1 * x))


def get_initial_values(integer):
    values = dict()
    for i in range(integer):
        values[i] = get_random_float()
    return values


def learn(w, eta, fault, x=1):
    return w + eta * fault * x


def get_final_output(y1, y2, w1, w2, w3, b1, b2):
    return (y1+y2) * w3 + b2


def get_actual_results_in_array(w1, w2, b, x_array):
    result = []
    for x in x_array:
        y1 = get_gaus_output(x, c1, r1)
        y2 = get_gaus_output(x, c2, r2)
        result.append(y1*w1 + y2*w2 + b)
    return result


def calculate():
    w1 = get_random_float()
    w2 = get_random_float()
    b = get_random_float()

    LEARNING_STEP = 0.1
    iterations = 0

    while iterations < 50000:
        for x in data:
            y1 = get_gaus_output(x, c1, r1)
            y2 = get_gaus_output(x, c2, r2)
            neuron_output = y1*w1 + y2*w2 + b

            desired = get_desired_result(x)
            fault = desired - neuron_output

            if fault != 0:
                w1 = learn(y1, LEARNING_STEP, fault, x)
                w2 = learn(y2, LEARNING_STEP, fault, x)
                b = learn(b, LEARNING_STEP, fault)
                continue

        iterations = iterations + 1

    return w1, w2, b


w1, w2, b = calculate()
plt.plot(data, get_desired_results_in_array(data))
data_for_validating = get_data_for_validating_results()
plt.scatter(data, get_actual_results_in_array(w1, w2, b, data))
plt.show()
